import { Component, OnInit , Input } from '@angular/core';
import { WorkerVO } from '../worker/WorkerVO'


@Component({
  selector: 'app-card-workers',
  templateUrl: './card-workers.component.html',
  styleUrls: ['./card-workers.component.css']
})
export class CardWorkersComponent implements OnInit {

  @Input()myCardWorker : WorkerVO = null;

  constructor() { }

  ngOnInit() {
  }

}
