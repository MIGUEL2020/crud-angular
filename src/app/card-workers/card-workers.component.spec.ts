import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardWorkersComponent } from './card-workers.component';

describe('CardWorkersComponent', () => {
  let component: CardWorkersComponent;
  let fixture: ComponentFixture<CardWorkersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardWorkersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardWorkersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
