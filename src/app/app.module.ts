import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import { NavbarComponentComponent } from './navbar-component/navbar-component.component';
import { FormularioComponentComponent } from './formulario-component/formulario-component.component';
import { TablaTrabajadoresComponentComponent } from './tabla-trabajadores-component/tabla-trabajadores-component.component';
import { CardWorkersComponent } from './card-workers/card-workers.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponentComponent,
    FormularioComponentComponent,
    TablaTrabajadoresComponentComponent,
    CardWorkersComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
