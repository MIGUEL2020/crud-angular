import { Component } from '@angular/core';
import { WorkerVO } from './worker/WorkerVO';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'CrudConListas';
  listWorkers : WorkerVO[] = [];
  mostrarCard = false;
  mostrarTabla = false;


  onAddWorker(worker : WorkerVO){
    this.listWorkers.push(worker);
    this.mostrarCard = true;
    this.mostrarTabla = false;
    console.log("add");
  }

  RecibirMostrarTabla(muestra : boolean){
    this.mostrarTabla = muestra;
    this.mostrarCard = false;
  }

}
