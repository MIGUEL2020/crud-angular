export class WorkerVO {

    name = "";
    lastName = "";
    age = null ;
    job = "";

  
    constructor(name:string,lastName:string,age:number,job:string){
        this.age=age;
        this.name=name;
        this.lastName=lastName;
        this.job=job;
    }

}