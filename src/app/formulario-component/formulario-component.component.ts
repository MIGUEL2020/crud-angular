import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { WorkerVO } from '../worker/WorkerVO'

@Component({
  selector: 'app-formulario-component',
  templateUrl: './formulario-component.component.html',
  styleUrls: ['./formulario-component.component.css'],
  
})
export class FormularioComponentComponent implements OnInit {
  
  @Output() workerDesdeHijo = new EventEmitter<WorkerVO>();
  @Output() pasarMostrarTabla = new EventEmitter();

  name = "";
   lastName = "";
   age = null ;
   job = "";

    //pos 0 = card y pos 1 = tabla
   mostrarComponentes = [];
   mostrarCard = false;

  ngOnInit() {
  }


  onAddWorker(){
    let myWorker = new WorkerVO(this.name, this.lastName, this.age,this.job);
    this.workerDesdeHijo.emit(myWorker);
  
  }

  onSeeWorkers(){
    this.pasarMostrarTabla.emit(true); 
  }

}
