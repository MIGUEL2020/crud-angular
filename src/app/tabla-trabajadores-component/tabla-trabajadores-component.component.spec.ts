import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaTrabajadoresComponentComponent } from './tabla-trabajadores-component.component';

describe('TablaTrabajadoresComponentComponent', () => {
  let component: TablaTrabajadoresComponentComponent;
  let fixture: ComponentFixture<TablaTrabajadoresComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaTrabajadoresComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaTrabajadoresComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
