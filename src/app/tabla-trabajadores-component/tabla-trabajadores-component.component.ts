import { Component, OnInit , Input } from '@angular/core';
import { WorkerVO } from '../worker/WorkerVO'
@Component({
  selector: 'app-tabla-trabajadores-component',
  templateUrl: './tabla-trabajadores-component.component.html',
  styleUrls: ['./tabla-trabajadores-component.component.css']
})
export class TablaTrabajadoresComponentComponent implements OnInit {

@Input()listWorkers : WorkerVO[]=[];
workerModal :WorkerVO;
posicionModificar = 0;

  ngOnInit() {
  }


  onHabilitarModificacion(worker : WorkerVO,i : number){
    this.workerModal = worker;
    this.posicionModificar = i;
  }

  onSave(){
    this.listWorkers[this.posicionModificar].name = ( <HTMLInputElement> document.getElementById("recipient-name")).value;;
    this.listWorkers[this.posicionModificar].lastName = ( <HTMLInputElement> document.getElementById("recipient-Lastname")).value;;
    this.listWorkers[this.posicionModificar].age = ( <HTMLInputElement> document.getElementById("recipient-age")).value;;
    this.listWorkers[this.posicionModificar].job = ( <HTMLInputElement> document.getElementById("recipient-job")).value;;

  }

  onDelete(i : number){
    if(confirm("Are u shure?")){
      this.listWorkers.splice(i,1);

    }
  }
  

  
}
